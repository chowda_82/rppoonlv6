﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_first
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook1 = new Notebook();
            notebook1.AddNote(new Note("rppoon-LV6", "Ivan Čordašić"));
            notebook1.AddNote(new Note("labos", "danas je suncan dan"));
            notebook1.AddNote(new Note("title", "tekst"));

            Iterator iterator = new Iterator(notebook1);
            iterator.Current.Show();
            iterator.Next().Show();

            while (iterator.IsDone != true)
            {
                iterator.Current.Show();
                iterator.Next();
            }
        }
    }
}
