﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_third
{
    class CareTaker
    {
        public List<Memento> PreviousState;
        public CareTaker()
        {
            this.PreviousState = new List<Memento>();
        }
        public void Add(Memento memento)
        {
            this.PreviousState.Add(memento);
        }
        public void Remove(Memento memento)
        {
            this.PreviousState.Remove(memento);
        }
        public void Clear(Memento memento)
        {
            this.PreviousState.Clear();
        }
        public Memento GetLastState()
        {
            Memento priv = PreviousState.Last();
            PreviousState.Remove(PreviousState.Last());
            return priv;
        }
    }
}
