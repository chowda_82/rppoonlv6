﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_third
{
    class Program
    {
        static void Main(string[] args)
        {
            CareTaker careTaker = new CareTaker();
            ToDoItem note1 = new ToDoItem("ZDK", "Napravi lv", DateTime.Parse("14.5.2020."));
            careTaker.Add(note1.StoreState());
            Console.WriteLine(note1.ToString());
            Console.WriteLine();
            note1.Rename("Pokušaj preimenovanja.");
            note1.ChangeTask("Idi van");
            note1.ChangeTimeDue(DateTime.Parse("13.5.2020."));
            careTaker.Add(note1.StoreState());
            Console.WriteLine(note1.ToString());
            note1.RestoreState(careTaker.GetLastState());
            Console.WriteLine(note1.ToString());
            Console.WriteLine();
            note1.RestoreState(careTaker.GetLastState());
            Console.WriteLine(note1.ToString());
        }
    }
}
