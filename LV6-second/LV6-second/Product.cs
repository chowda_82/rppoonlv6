﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace LV6_second
{
    class Product
    {
        public string Description { get; private set; }
        public double Price { get; private set; }
        public Product(string description, double price)
        {
            this.Description = description;
            this.Price = price;
        }
        public override string ToString()
        {
            return this.Description + ":\n" + this.Price;
        }
        private int GetFrameWidth()
        {
            return this.Description.Length < this.Price.ToString().Length ? this.Price.ToString().Length : this.Description.Length;
        }
        private string GetRule(char x)
        {
            return new string(x, this.GetFrameWidth());
        }
        public void Show()
        {
            Console.WriteLine(this.GetRule('='));
            Console.WriteLine(this.Description);
            Console.WriteLine(this.GetRule('-'));
            Console.WriteLine(this.Price);
            Console.WriteLine(this.GetRule('='));
        }
    }
}
