﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_second
{
    class Program
    {
        static void Main(string[] args)
        {
            Product book= new Product("20000 milja", 199);
            Product movie = new Product("Shawshank", 5.98);
            Box box = new Box();
            box.AddProduct(book);
            box.AddProduct(movie);
            Iterator iterator = (Iterator)box.GetIterator();
            while(!iterator.IsDone)
            {
                iterator.Current.Show();
                iterator.Next();
            }
            box.RemoveProduct(movie);
            while(!iterator.IsDone)
            { 
                iterator.Current.Show();
                iterator.Next();
            }
        }
    }
}
